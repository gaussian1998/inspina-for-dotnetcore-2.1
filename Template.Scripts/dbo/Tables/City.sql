﻿CREATE TABLE [dbo].[City] (
    [Id]   VARCHAR(2) NOT NULL,
	[Name] nvarchar(10) NOT NULL,
	CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ([Id] ASC)
);
