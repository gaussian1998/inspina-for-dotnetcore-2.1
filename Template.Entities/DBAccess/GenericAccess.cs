﻿using ClassLibrary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;


namespace Template.Entities.DBAccess
{
    public class GenericAccess<DbContextSubType, Entity>
                            where DbContextSubType : DbContext, new()
                            where Entity : class, new()
    {
        public static List<Entity> Find(Expression<Func<Entity, bool>> predicate)
        {
            using (DbContextSubType db = new DbContextSubType())
            {
                return db.Set<Entity>().Where(predicate).ToList();
            }
        }

        public static IEnumerable<Entity> Find(Expression<Func<Entity, bool>> predicate, DbContextSubType db)
        {
            return db.Set<Entity>().Where(predicate);
        }

        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
        {
            using (DbContextSubType db = new DbContextSubType())
            {
                return db.Set<Entity>().Where(predicate).Select(select).ToList();
            }
        }

        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate)
            where T : new()
        {
            using (DbContextSubType db = new DbContextSubType())
            {
                var query = db.Set<Entity>().Where(predicate).ToList();

                return MapProperty.MapAll<T,Entity>(query).ToList();
            }
        }

    }
}
