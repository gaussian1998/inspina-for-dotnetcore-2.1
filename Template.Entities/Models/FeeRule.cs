﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class FeeRule
    {
        public FeeRule()
        {
            ServiceXfee = new HashSet<ServiceXfee>();
        }

        public long FeeRuleId { get; set; }
        public string FeeName { get; set; }
        public string FeeRuleCode { get; set; }
        public int PaymentCycleCode { get; set; }
        public int CalculationModeCode { get; set; }
        public int? CumulativeBenchmarkStart { get; set; }
        public int? CumulativeBenchmarkEnd { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<ServiceXfee> ServiceXfee { get; set; }
    }
}
