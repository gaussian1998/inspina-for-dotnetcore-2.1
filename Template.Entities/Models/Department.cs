﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Department
    {
        public long DepartmentId { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
