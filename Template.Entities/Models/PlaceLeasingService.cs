﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class PlaceLeasingService
    {
        public PlaceLeasingService()
        {
            TenancyReservationDetail = new HashSet<TenancyReservationDetail>();
        }

        public string PlaceLeasingServiceId { get; set; }
        public string PlaceLeasingRecordId { get; set; }
        public long ServiceClassId { get; set; }
        public decimal BaseMonthlyRental { get; set; }
        public decimal Deposit { get; set; }
        public bool IsClose { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public PlaceLeasingRecord PlaceLeasingRecord { get; set; }
        public ServiceClass ServiceClass { get; set; }
        public ICollection<TenancyReservationDetail> TenancyReservationDetail { get; set; }
    }
}
