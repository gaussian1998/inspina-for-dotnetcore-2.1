﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ServiceOrderDetail
    {
        public int ServiceOrderDetailId { get; set; }
        public string ServiceOrderId { get; set; }
        public long FeeRuleId { get; set; }
        public string FeeName { get; set; }
        public decimal? Fee { get; set; }
        public decimal? Rate { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ServiceOrder ServiceOrder { get; set; }
    }
}
