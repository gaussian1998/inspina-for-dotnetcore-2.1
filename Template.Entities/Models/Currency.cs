﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Currency
    {
        public Currency()
        {
            OwnerExposureDeposit = new HashSet<OwnerExposureDeposit>();
            StoreExposureDeposit = new HashSet<StoreExposureDeposit>();
        }

        public int CurrencyId { get; set; }
        public string Type { get; set; }

        public ICollection<OwnerExposureDeposit> OwnerExposureDeposit { get; set; }
        public ICollection<StoreExposureDeposit> StoreExposureDeposit { get; set; }
    }
}
