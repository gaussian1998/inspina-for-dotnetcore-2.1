﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Account
    {
        public string AccountId { get; set; }
        public int OwnerId { get; set; }
        public string UserAccount { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string Phone { get; set; }
        public bool? IsActive { get; set; }
        public string Address { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Owner Owner { get; set; }
    }
}
