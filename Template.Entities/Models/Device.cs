﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Device
    {
        public Device()
        {
            MemberDevice = new HashSet<MemberDevice>();
            StoreClawMachinePlace = new HashSet<StoreClawMachinePlace>();
        }

        public long DeviceId { get; set; }
        public long StoreId { get; set; }
        public long MachineId { get; set; }
        public string DeviceNumber { get; set; }
        public string PropertyNumber { get; set; }
        public int SerialNumber { get; set; }
        public bool IsScrapped { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExFactoryDate { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Machine Machine { get; set; }
        public Store Store { get; set; }
        public ICollection<MemberDevice> MemberDevice { get; set; }
        public ICollection<StoreClawMachinePlace> StoreClawMachinePlace { get; set; }
    }
}
