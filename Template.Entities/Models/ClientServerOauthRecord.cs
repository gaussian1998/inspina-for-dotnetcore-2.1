﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ClientServerOauthRecord
    {
        public ClientServerOauthRecord()
        {
            ClientToProtectedResource = new HashSet<ClientToProtectedResource>();
        }

        public string ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string ClientKey { get; set; }
        public string ClientIv { get; set; }
        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }
        public DateTime CreatTime { get; set; }
        public DateTime UpdateTime { get; set; }

        public ICollection<ClientToProtectedResource> ClientToProtectedResource { get; set; }
    }
}
