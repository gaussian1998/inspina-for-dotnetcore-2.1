﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Template.Entities.Models
{
    public partial class Renting_DBContext : DbContext
    {
        public Renting_DBContext()
        {
        }

        public Renting_DBContext(DbContextOptions<Renting_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountXrole> AccountXrole { get; set; }
        public virtual DbSet<AppointmentStatusId> AppointmentStatusId { get; set; }
        public virtual DbSet<CalculationMode> CalculationMode { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<ClientServerOauthRecord> ClientServerOauthRecord { get; set; }
        public virtual DbSet<ClientToProtectedResource> ClientToProtectedResource { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<FeeRule> FeeRule { get; set; }
        public virtual DbSet<FileSystem> FileSystem { get; set; }
        public virtual DbSet<GroundRule> GroundRule { get; set; }
        public virtual DbSet<LeaseAnnounce> LeaseAnnounce { get; set; }
        public virtual DbSet<LeaseAnnounceXgroundRule> LeaseAnnounceXgroundRule { get; set; }
        public virtual DbSet<Machine> Machine { get; set; }
        public virtual DbSet<MachineType> MachineType { get; set; }
        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<MemberDevice> MemberDevice { get; set; }
        public virtual DbSet<Owner> Owner { get; set; }
        public virtual DbSet<OwnerExposureDeposit> OwnerExposureDeposit { get; set; }
        public virtual DbSet<OwnerExposureDepositLog> OwnerExposureDepositLog { get; set; }
        public virtual DbSet<PaymentCycle> PaymentCycle { get; set; }
        public virtual DbSet<PlaceLeasingRecord> PlaceLeasingRecord { get; set; }
        public virtual DbSet<PlaceLeasingService> PlaceLeasingService { get; set; }
        public virtual DbSet<PropertyIdChangeRecord> PropertyIdChangeRecord { get; set; }
        public virtual DbSet<ProtectedServer> ProtectedServer { get; set; }
        public virtual DbSet<ProtectedServerDetail> ProtectedServerDetail { get; set; }
        public virtual DbSet<Rank> Rank { get; set; }
        public virtual DbSet<ServiceClass> ServiceClass { get; set; }
        public virtual DbSet<ServiceOrder> ServiceOrder { get; set; }
        public virtual DbSet<ServiceOrderDetail> ServiceOrderDetail { get; set; }
        public virtual DbSet<ServiceOrdertStatus> ServiceOrdertStatus { get; set; }
        public virtual DbSet<ServiceXfee> ServiceXfee { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<StoreClawMachinePlace> StoreClawMachinePlace { get; set; }
        public virtual DbSet<StoreExposureDeposit> StoreExposureDeposit { get; set; }
        public virtual DbSet<StoreExposureDepositLog> StoreExposureDepositLog { get; set; }
        public virtual DbSet<StoreXequipment> StoreXequipment { get; set; }
        public virtual DbSet<TenancyReservation> TenancyReservation { get; set; }
        public virtual DbSet<TenancyReservationDetail> TenancyReservationDetail { get; set; }
        public virtual DbSet<Town> Town { get; set; }
        public virtual DbSet<Translation> Translation { get; set; }
        public virtual DbSet<ValidityPeriod> ValidityPeriod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("data source=.\\SQLEXPRESS;initial catalog=Renting_DB;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.AccountId)
                    .HasMaxLength(32)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.CellPhone).HasMaxLength(32);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Phone).HasMaxLength(32);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.UserAccount)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Account_OwnerId");
            });

            modelBuilder.Entity<AccountXrole>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.RoleId });

                entity.ToTable("AccountXRole");

                entity.Property(e => e.AccountId).HasMaxLength(32);

                entity.Property(e => e.RoleId).HasMaxLength(32);
            });

            modelBuilder.Entity<AppointmentStatusId>(entity =>
            {
                entity.HasKey(e => e.AppointmentStatusId1);

                entity.Property(e => e.AppointmentStatusId1)
                    .HasColumnName("AppointmentStatusId")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<CalculationMode>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.Property(e => e.Code).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(128);
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<ClientServerOauthRecord>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.ToTable("ClientServerOAuthRecord");

                entity.Property(e => e.ClientId)
                    .HasMaxLength(32)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientCode)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.Property(e => e.ClientIv)
                    .IsRequired()
                    .HasColumnName("ClientIV");

                entity.Property(e => e.ClientKey).IsRequired();

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.CreatTime).HasColumnType("datetime");

                entity.Property(e => e.CreateUser)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasDefaultValueSql("('System')");

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasDefaultValueSql("('System')");
            });

            modelBuilder.Entity<ClientToProtectedResource>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AuthZtimes).HasColumnName("AuthZTimes");

                entity.Property(e => e.Avaliable).HasDefaultValueSql("((0))");

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.ClientProtectedIv).HasColumnName("ClientProtectedIV");

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.ProtectedResourceApiUrl).IsRequired();

                entity.Property(e => e.ProtectedResourceId)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.ProtectedResourceName)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.ProtectedResourceUrl).IsRequired();

                entity.Property(e => e.RandomValue).HasMaxLength(64);

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientToProtectedResource)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK_ClientToProtectedResource_ClientServerOAuthRecord");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.CurrencyId).ValueGeneratedNever();

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.ShortCode).HasMaxLength(1);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.HasIndex(e => e.DeviceNumber)
                    .HasName("UX_DEVICE_DeviceNumber")
                    .IsUnique();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DeviceNumber)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.ExFactoryDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyNumber).HasMaxLength(24);

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Device)
                    .HasForeignKey(d => d.MachineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DEVICE_REFERENCE_MACHINE");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Device)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DEVICE_REFERENCE_STORE");
            });

            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.Property(e => e.CodeId)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<FeeRule>(entity =>
            {
                entity.Property(e => e.FeeRuleId).ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.FeeName)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.FeeRuleCode).HasMaxLength(3);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<FileSystem>(entity =>
            {
                entity.ToTable("File_System");

                entity.HasIndex(e => e.FileName);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Directory)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(384);

                entity.HasOne(d => d.DirectoryNavigation)
                    .WithMany(p => p.InverseDirectoryNavigation)
                    .HasForeignKey(d => d.Directory)
                    .HasConstraintName("FK_File_System_Self");
            });

            modelBuilder.Entity<GroundRule>(entity =>
            {
                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(40);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<LeaseAnnounce>(entity =>
            {
                entity.HasKey(e => e.StoreId);

                entity.Property(e => e.StoreId).ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ExpectedOpeningDate).HasMaxLength(20);

                entity.Property(e => e.IsClosed)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReleaseDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(40);

                entity.Property(e => e.RentModeIntroduction).HasMaxLength(200);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Rank)
                    .WithMany(p => p.LeaseAnnounce)
                    .HasForeignKey(d => d.RankId)
                    .HasConstraintName("FK_LEASEANN_REFERENCE_RANK");

                entity.HasOne(d => d.Store)
                    .WithOne(p => p.LeaseAnnounce)
                    .HasForeignKey<LeaseAnnounce>(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LEASEANN_REFERENCE_STORE");
            });

            modelBuilder.Entity<LeaseAnnounceXgroundRule>(entity =>
            {
                entity.HasKey(e => new { e.GroundRuleId, e.StoreId });

                entity.ToTable("LeaseAnnounceXGroundRule");

                entity.HasOne(d => d.GroundRule)
                    .WithMany(p => p.LeaseAnnounceXgroundRule)
                    .HasForeignKey(d => d.GroundRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LEASEANN_REFERENCE_GROUNDRU");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.LeaseAnnounceXgroundRule)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LEASEANN_REFERENCE_LEASEANN");
            });

            modelBuilder.Entity<Machine>(entity =>
            {
                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DeviceName).HasMaxLength(50);

                entity.Property(e => e.Manufacturer)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.ModelNumber).HasMaxLength(10);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.MachineType)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.MachineTypeId)
                    .HasConstraintName("FK_MACHINE_REFERENCE_MACHINET");
            });

            modelBuilder.Entity<MachineType>(entity =>
            {
                entity.Property(e => e.MachineTypeId).ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Describe).HasMaxLength(100);

                entity.Property(e => e.MachineTypeNo).HasMaxLength(3);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Member>(entity =>
            {
                entity.HasIndex(e => e.Phone)
                    .HasName("AK_Member_Phone")
                    .IsUnique();

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.BackofIdcardImage)
                    .HasColumnName("BackofIDCardImage")
                    .HasMaxLength(250);

                entity.Property(e => e.Birthday).HasColumnType("datetime");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.FrontofIdcardImage)
                    .HasColumnName("FrontofIDCardImage")
                    .HasMaxLength(250);

                entity.Property(e => e.Gender).HasMaxLength(10);

                entity.Property(e => e.IdentityNo).HasMaxLength(32);

                entity.Property(e => e.MemberAccount)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.MemberImage).HasMaxLength(250);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.NickName).HasMaxLength(128);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Rank)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Representative).HasMaxLength(128);

                entity.Property(e => e.Tel).HasMaxLength(32);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MemberDevice>(entity =>
            {
                entity.HasIndex(e => new { e.DeviceId, e.MemberId })
                    .HasName("Index_1")
                    .IsUnique();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.MemberDevice)
                    .HasForeignKey(d => d.DeviceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MEMBERDE_REFERENCE_DEVICE");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberDevice)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MEMBERDE_REFERENCE_MEMBER");
            });

            modelBuilder.Entity<Owner>(entity =>
            {
                entity.HasIndex(e => e.ShortCode)
                    .HasName("UK_OWNER_ShortCode")
                    .IsUnique();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Phone)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.UniformNumber)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<OwnerExposureDeposit>(entity =>
            {
                entity.HasIndex(e => new { e.OwnerId, e.CurrencyId })
                    .HasName("AK_OwnerExposureDeposit_OwnerId_CurrencyId")
                    .IsUnique();

                entity.Property(e => e.OwnerExposureDepositId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.OwnerExposureDeposit)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OWNEREXP_REFERENCE_CURRENCY");

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.OwnerExposureDeposit)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OWNEREXP_REFERENCE_OWNER");
            });

            modelBuilder.Entity<OwnerExposureDepositLog>(entity =>
            {
                entity.HasKey(e => new { e.OwnerExposureDepositLogId, e.OwnerExposureDepositId });

                entity.Property(e => e.OwnerExposureDepositId)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.OwnerExposureDeposit)
                    .WithMany(p => p.OwnerExposureDepositLog)
                    .HasForeignKey(d => d.OwnerExposureDepositId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OwnerExposureDepositLog_OwnerExposureDeposit");
            });

            modelBuilder.Entity<PaymentCycle>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.Property(e => e.Code).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(128);
            });

            modelBuilder.Entity<PlaceLeasingRecord>(entity =>
            {
                entity.Property(e => e.PlaceLeasingRecordId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DecorationTime).HasMaxLength(20);

                entity.Property(e => e.PlaceLeasingRecordNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Place)
                    .WithMany(p => p.PlaceLeasingRecord)
                    .HasForeignKey(d => d.PlaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STOREPLA_REFERENCE_STORECLA");
            });

            modelBuilder.Entity<PlaceLeasingService>(entity =>
            {
                entity.Property(e => e.PlaceLeasingServiceId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BaseMonthlyRental).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Deposit).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PlaceLeasingRecordId)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.PlaceLeasingRecord)
                    .WithMany(p => p.PlaceLeasingService)
                    .HasForeignKey(d => d.PlaceLeasingRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PLACELEA_REFERENCE_PLACELEA");

                entity.HasOne(d => d.ServiceClass)
                    .WithMany(p => p.PlaceLeasingService)
                    .HasForeignKey(d => d.ServiceClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlaceLeasingService_ServiceClass");
            });

            modelBuilder.Entity<PropertyIdChangeRecord>(entity =>
            {
                entity.HasKey(e => e.Picid);

                entity.Property(e => e.Picid).HasColumnName("PICId");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.NewPropertyId).HasMaxLength(24);

                entity.Property(e => e.OldPropertyId)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<ProtectedServer>(entity =>
            {
                entity.HasKey(e => e.ProtectedId);

                entity.HasIndex(e => e.AppName)
                    .HasName("UQ__Protecte__B0EA68B959EF96A8")
                    .IsUnique();

                entity.Property(e => e.ProtectedId)
                    .HasMaxLength(32)
                    .ValueGeneratedNever();

                entity.Property(e => e.AppName)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.AuthProtectedIv)
                    .IsRequired()
                    .HasColumnName("AuthProtectedIV");

                entity.Property(e => e.AuthProtectedKey).IsRequired();

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProtectedServerDetail>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AuthZtimes).HasColumnName("AuthZTimes");

                entity.Property(e => e.Avaliable).HasDefaultValueSql("((0))");

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.ClientProtectedIv)
                    .IsRequired()
                    .HasColumnName("ClientProtectedIV");

                entity.Property(e => e.ClientProtectedKey).IsRequired();

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.HashValue).IsRequired();

                entity.Property(e => e.ProtectedId)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Protected)
                    .WithMany(p => p.ProtectedServerDetail)
                    .HasForeignKey(d => d.ProtectedId)
                    .HasConstraintName("FK_ProtectedServerDetail_ProtectedServer");
            });

            modelBuilder.Entity<Rank>(entity =>
            {
                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Remark).HasMaxLength(40);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<ServiceClass>(entity =>
            {
                entity.Property(e => e.ServiceClassId).ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.HasOne(d => d.ValidityPeriodCodeNavigation)
                    .WithMany(p => p.ServiceClass)
                    .HasForeignKey(d => d.ValidityPeriodCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceClass_ValidityPeriod");
            });

            modelBuilder.Entity<ServiceOrder>(entity =>
            {
                entity.Property(e => e.ServiceOrderId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CancelingDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DeviceContractImage).HasMaxLength(250);

                entity.Property(e => e.DeviceNumber)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.OwnerCode)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.ServiceEndingDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceOrderNumber)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.ServiceStartingDate).HasColumnType("datetime");

                entity.Property(e => e.StoreName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator).HasMaxLength(20);

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.ServiceOrder)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICEO_REFERENCE_MEMBER");

                entity.HasOne(d => d.Place)
                    .WithMany(p => p.ServiceOrder)
                    .HasForeignKey(d => d.PlaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceOrder_StoreClawMachinePlace");

                entity.HasOne(d => d.ServiceOrdertStatus)
                    .WithMany(p => p.ServiceOrder)
                    .HasForeignKey(d => d.ServiceOrdertStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceOrder_ServiceOrdertStatus");
            });

            modelBuilder.Entity<ServiceOrderDetail>(entity =>
            {
                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Fee).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FeeName)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ServiceOrderId)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.HasOne(d => d.ServiceOrder)
                    .WithMany(p => p.ServiceOrderDetail)
                    .HasForeignKey(d => d.ServiceOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICEO_REFERENCE_SERVICEO");
            });

            modelBuilder.Entity<ServiceOrdertStatus>(entity =>
            {
                entity.Property(e => e.ServiceOrdertStatusId).ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<ServiceXfee>(entity =>
            {
                entity.ToTable("ServiceXFee");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.HasOne(d => d.FeeRule)
                    .WithMany(p => p.ServiceXfee)
                    .HasForeignKey(d => d.FeeRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICEX_REFERENCE_FEERULE");

                entity.HasOne(d => d.ServiceClass)
                    .WithMany(p => p.ServiceXfee)
                    .HasForeignKey(d => d.ServiceClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICEX_REFERENCE_SERVICEC");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.HasIndex(e => new { e.OwnerId, e.OwnerSerial })
                    .HasName("UX_STORE_OwnerId_OwnerSerial")
                    .IsUnique();

                entity.Property(e => e.BusinessHours)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Describe).HasMaxLength(200);

                entity.Property(e => e.LayoutImage).HasMaxLength(250);

                entity.Property(e => e.LocationContractImage).HasMaxLength(250);

                entity.Property(e => e.LocationExpiredDate).HasColumnType("datetime");

                entity.Property(e => e.ManagerName).HasMaxLength(20);

                entity.Property(e => e.ManagerPhone).HasMaxLength(15);

                entity.Property(e => e.OpeningDate).HasColumnType("datetime");

                entity.Property(e => e.Road)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.StoreImage).HasMaxLength(250);

                entity.Property(e => e.StoreName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.StoreNumber)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Store)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STORE_REFERENCE_OWNER");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.Store)
                    .HasForeignKey(d => d.TownId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STORE_REFERENCE_Town");
            });

            modelBuilder.Entity<StoreClawMachinePlace>(entity =>
            {
                entity.HasKey(e => e.PlaceId);

                entity.HasIndex(e => new { e.StoreId, e.DeviceId, e.PlaceNumber })
                    .HasName("Index_unique")
                    .IsUnique();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.StoreClawMachinePlace)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_STORECLA_REFERENCE_DEVICE");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.StoreClawMachinePlace)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STORECLA_REFERENCE_STORE");
            });

            modelBuilder.Entity<StoreExposureDeposit>(entity =>
            {
                entity.HasIndex(e => new { e.StoreId, e.CurrencyId })
                    .HasName("AK_StoreExposureDeposit_StoreId_CurrencyId")
                    .IsUnique();

                entity.Property(e => e.StoreExposureDepositId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.StoreExposureDeposit)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STOREEXP_REFERENCE_CURRENCY");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.StoreExposureDeposit)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STOREEXP_REFERENCE_STORE");
            });

            modelBuilder.Entity<StoreExposureDepositLog>(entity =>
            {
                entity.HasKey(e => new { e.StoreExposureDepositLogId, e.StoreExposureDepositId });

                entity.Property(e => e.StoreExposureDepositId)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.StoreExposureDeposit)
                    .WithMany(p => p.StoreExposureDepositLog)
                    .HasForeignKey(d => d.StoreExposureDepositId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoreExposureDepositLog_StoreExposureDeposit");
            });

            modelBuilder.Entity<StoreXequipment>(entity =>
            {
                entity.HasKey(e => new { e.StoreId, e.EquipmentId });

                entity.ToTable("StoreXEquipment");

                entity.HasOne(d => d.Equipment)
                    .WithMany(p => p.StoreXequipment)
                    .HasForeignKey(d => d.EquipmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STOREXEQ_REFERENCE_EQUIPMEN");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.StoreXequipment)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_STOREXEQ_REFERENCE_STORE");
            });

            modelBuilder.Entity<TenancyReservation>(entity =>
            {
                entity.Property(e => e.TenancyReservationId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.Remark).HasMaxLength(512);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator).HasMaxLength(20);

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.TenancyReservation)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TENANCYR_REFERENCE_MEMBER");
            });

            modelBuilder.Entity<TenancyReservationDetail>(entity =>
            {
                entity.Property(e => e.TenancyReservationDetailId)
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AppointmentDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.PlaceLeasingServiceId)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.RentIncrease).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ServiceOrderId)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.TenancyReservationId)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator).HasMaxLength(20);

                entity.HasOne(d => d.AppointmentStatus)
                    .WithMany(p => p.TenancyReservationDetail)
                    .HasForeignKey(d => d.AppointmentStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TENANCYR_REFERENCE_K");

                entity.HasOne(d => d.PlaceLeasingService)
                    .WithMany(p => p.TenancyReservationDetail)
                    .HasForeignKey(d => d.PlaceLeasingServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TENANCYR_REFERENCE_PLACELEA");

                entity.HasOne(d => d.TenancyReservation)
                    .WithMany(p => p.TenancyReservationDetail)
                    .HasForeignKey(d => d.TenancyReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TENANCYR_REFERENCE_TENANCYR");
            });

            modelBuilder.Entity<Town>(entity =>
            {
                entity.Property(e => e.CityId)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Town)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Town_REFERENCE_City");
            });

            modelBuilder.Entity<Translation>(entity =>
            {
                entity.Property(e => e.CodeId).IsRequired();

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Updator)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<ValidityPeriod>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.Property(e => e.Code).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(128);
            });
        }
    }
}
