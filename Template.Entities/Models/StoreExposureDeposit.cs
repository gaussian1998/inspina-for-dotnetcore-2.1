﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class StoreExposureDeposit
    {
        public StoreExposureDeposit()
        {
            StoreExposureDepositLog = new HashSet<StoreExposureDepositLog>();
        }

        public string StoreExposureDepositId { get; set; }
        public long StoreId { get; set; }
        public int CurrencyId { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public Currency Currency { get; set; }
        public Store Store { get; set; }
        public ICollection<StoreExposureDepositLog> StoreExposureDepositLog { get; set; }
    }
}
