﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class OwnerExposureDeposit
    {
        public OwnerExposureDeposit()
        {
            OwnerExposureDepositLog = new HashSet<OwnerExposureDepositLog>();
        }

        public string OwnerExposureDepositId { get; set; }
        public int OwnerId { get; set; }
        public int CurrencyId { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public Currency Currency { get; set; }
        public Owner Owner { get; set; }
        public ICollection<OwnerExposureDepositLog> OwnerExposureDepositLog { get; set; }
    }
}
