﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class MachineType
    {
        public MachineType()
        {
            Machine = new HashSet<Machine>();
        }

        public long MachineTypeId { get; set; }
        public string MachineTypeNo { get; set; }
        public string Name { get; set; }
        public string Describe { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<Machine> Machine { get; set; }
    }
}
