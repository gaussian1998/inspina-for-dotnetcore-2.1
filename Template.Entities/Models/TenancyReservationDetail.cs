﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class TenancyReservationDetail
    {
        public string TenancyReservationDetailId { get; set; }
        public string PlaceLeasingServiceId { get; set; }
        public string TenancyReservationId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public int AppointmentStatusId { get; set; }
        public decimal? RentIncrease { get; set; }
        public string ServiceOrderId { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime? UpdateDateTime { get; set; }

        public AppointmentStatusId AppointmentStatus { get; set; }
        public PlaceLeasingService PlaceLeasingService { get; set; }
        public TenancyReservation TenancyReservation { get; set; }
    }
}
