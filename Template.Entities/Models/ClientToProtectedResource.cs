﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ClientToProtectedResource
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ProtectedResourceId { get; set; }
        public string ProtectedResourceName { get; set; }
        public string ProtectedResourceUrl { get; set; }
        public string ProtectedResourceApiUrl { get; set; }
        public string HashValue { get; set; }
        public string RandomValue { get; set; }
        public int? AuthZtimes { get; set; }
        public int? CurrentTimes { get; set; }
        public string ClientProtectedKey { get; set; }
        public string ClientProtectedIv { get; set; }
        public string ValueStringList { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? Avaliable { get; set; }

        public ClientServerOauthRecord Client { get; set; }
    }
}
