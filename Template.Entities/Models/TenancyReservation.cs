﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class TenancyReservation
    {
        public TenancyReservation()
        {
            TenancyReservationDetail = new HashSet<TenancyReservationDetail>();
        }

        public string TenancyReservationId { get; set; }
        public long MemberId { get; set; }
        public int? AmountOfRent { get; set; }
        public string Remark { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime? UpdateDateTime { get; set; }

        public Member Member { get; set; }
        public ICollection<TenancyReservationDetail> TenancyReservationDetail { get; set; }
    }
}
