﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class MemberDevice
    {
        public long MemberDeviceId { get; set; }
        public long DeviceId { get; set; }
        public long MemberId { get; set; }
        public bool IsMainRole { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Device Device { get; set; }
        public Member Member { get; set; }
    }
}
