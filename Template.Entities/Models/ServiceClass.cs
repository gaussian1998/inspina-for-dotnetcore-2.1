﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ServiceClass
    {
        public ServiceClass()
        {
            PlaceLeasingService = new HashSet<PlaceLeasingService>();
            ServiceXfee = new HashSet<ServiceXfee>();
        }

        public long ServiceClassId { get; set; }
        public string ServiceName { get; set; }
        public int ValidityPeriodCode { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ValidityPeriod ValidityPeriodCodeNavigation { get; set; }
        public ICollection<PlaceLeasingService> PlaceLeasingService { get; set; }
        public ICollection<ServiceXfee> ServiceXfee { get; set; }
    }
}
