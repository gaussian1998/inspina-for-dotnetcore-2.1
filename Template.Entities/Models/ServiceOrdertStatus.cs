﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ServiceOrdertStatus
    {
        public ServiceOrdertStatus()
        {
            ServiceOrder = new HashSet<ServiceOrder>();
        }

        public int ServiceOrdertStatusId { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<ServiceOrder> ServiceOrder { get; set; }
    }
}
