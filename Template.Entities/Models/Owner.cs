﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Owner
    {
        public Owner()
        {
            Account = new HashSet<Account>();
            OwnerExposureDeposit = new HashSet<OwnerExposureDeposit>();
            Store = new HashSet<Store>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
        public string UniformNumber { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<Account> Account { get; set; }
        public ICollection<OwnerExposureDeposit> OwnerExposureDeposit { get; set; }
        public ICollection<Store> Store { get; set; }
    }
}
