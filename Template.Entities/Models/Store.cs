﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Store
    {
        public Store()
        {
            Device = new HashSet<Device>();
            StoreClawMachinePlace = new HashSet<StoreClawMachinePlace>();
            StoreExposureDeposit = new HashSet<StoreExposureDeposit>();
            StoreXequipment = new HashSet<StoreXequipment>();
        }

        public long StoreId { get; set; }
        public int OwnerId { get; set; }
        public int TownId { get; set; }
        public string Road { get; set; }
        public string StoreNumber { get; set; }
        public string StoreName { get; set; }
        public int OwnerSerial { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Describe { get; set; }
        public string BusinessHours { get; set; }
        public bool IsWarehouse { get; set; }
        public DateTime? OpeningDate { get; set; }
        public bool IsClosed { get; set; }
        public string ManagerName { get; set; }
        public string ManagerPhone { get; set; }
        public DateTime? LocationExpiredDate { get; set; }
        public string LocationContractImage { get; set; }
        public string LayoutImage { get; set; }
        public string StoreImage { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Owner Owner { get; set; }
        public Town Town { get; set; }
        public LeaseAnnounce LeaseAnnounce { get; set; }
        public ICollection<Device> Device { get; set; }
        public ICollection<StoreClawMachinePlace> StoreClawMachinePlace { get; set; }
        public ICollection<StoreExposureDeposit> StoreExposureDeposit { get; set; }
        public ICollection<StoreXequipment> StoreXequipment { get; set; }
    }
}
