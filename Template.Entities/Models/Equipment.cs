﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Equipment
    {
        public Equipment()
        {
            StoreXequipment = new HashSet<StoreXequipment>();
        }

        public long EquipmentId { get; set; }
        public string CodeId { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<StoreXequipment> StoreXequipment { get; set; }
    }
}
