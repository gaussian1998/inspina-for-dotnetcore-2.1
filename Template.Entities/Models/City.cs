﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class City
    {
        public City()
        {
            Town = new HashSet<Town>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public ICollection<Town> Town { get; set; }
    }
}
