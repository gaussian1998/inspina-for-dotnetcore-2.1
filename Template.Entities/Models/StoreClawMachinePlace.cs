﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class StoreClawMachinePlace
    {
        public StoreClawMachinePlace()
        {
            PlaceLeasingRecord = new HashSet<PlaceLeasingRecord>();
            ServiceOrder = new HashSet<ServiceOrder>();
        }

        public long PlaceId { get; set; }
        public long StoreId { get; set; }
        public long? DeviceId { get; set; }
        public int PlaceNumber { get; set; }
        public string Description { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Device Device { get; set; }
        public Store Store { get; set; }
        public ICollection<PlaceLeasingRecord> PlaceLeasingRecord { get; set; }
        public ICollection<ServiceOrder> ServiceOrder { get; set; }
    }
}
