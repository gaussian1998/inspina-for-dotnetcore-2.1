﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Translation
    {
        public long TranslationId { get; set; }
        public string CodeId { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
