﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class GroundRule
    {
        public GroundRule()
        {
            LeaseAnnounceXgroundRule = new HashSet<LeaseAnnounceXgroundRule>();
        }

        public long GroundRuleId { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<LeaseAnnounceXgroundRule> LeaseAnnounceXgroundRule { get; set; }
    }
}
