﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Town
    {
        public Town()
        {
            Store = new HashSet<Store>();
        }

        public int Id { get; set; }
        public string ZipCode { get; set; }
        public string Name { get; set; }
        public string CityId { get; set; }

        public City City { get; set; }
        public ICollection<Store> Store { get; set; }
    }
}
