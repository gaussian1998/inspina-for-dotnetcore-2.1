﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Rank
    {
        public Rank()
        {
            LeaseAnnounce = new HashSet<LeaseAnnounce>();
        }

        public long RankId { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<LeaseAnnounce> LeaseAnnounce { get; set; }
    }
}
