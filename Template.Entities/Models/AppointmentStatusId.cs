﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class AppointmentStatusId
    {
        public AppointmentStatusId()
        {
            TenancyReservationDetail = new HashSet<TenancyReservationDetail>();
        }

        public int AppointmentStatusId1 { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<TenancyReservationDetail> TenancyReservationDetail { get; set; }
    }
}
