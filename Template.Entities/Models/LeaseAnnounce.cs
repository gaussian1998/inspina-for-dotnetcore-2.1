﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class LeaseAnnounce
    {
        public LeaseAnnounce()
        {
            LeaseAnnounceXgroundRule = new HashSet<LeaseAnnounceXgroundRule>();
        }

        public long StoreId { get; set; }
        public long? RankId { get; set; }
        public string Title { get; set; }
        public string ExpectedOpeningDate { get; set; }
        public string RentModeIntroduction { get; set; }
        public int? ExposureWeights { get; set; }
        public string Remark { get; set; }
        public bool? IsClosed { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Rank Rank { get; set; }
        public Store Store { get; set; }
        public ICollection<LeaseAnnounceXgroundRule> LeaseAnnounceXgroundRule { get; set; }
    }
}
