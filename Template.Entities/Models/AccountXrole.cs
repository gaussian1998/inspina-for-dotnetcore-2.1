﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class AccountXrole
    {
        public string AccountId { get; set; }
        public string RoleId { get; set; }
    }
}
