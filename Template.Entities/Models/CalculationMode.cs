﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class CalculationMode
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public int? Value { get; set; }
    }
}
