﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ServiceOrder
    {
        public ServiceOrder()
        {
            ServiceOrderDetail = new HashSet<ServiceOrderDetail>();
        }

        public string ServiceOrderId { get; set; }
        public int ServiceOrdertStatusId { get; set; }
        public long MemberId { get; set; }
        public long ServiceClassId { get; set; }
        public long? StorePlaceLeasingRecordId { get; set; }
        public string ServiceOrderNumber { get; set; }
        public string StoreName { get; set; }
        public long PlaceId { get; set; }
        public int PlaceNumber { get; set; }
        public string DeviceNumber { get; set; }
        public string OwnerCode { get; set; }
        public int SerialNumber { get; set; }
        public DateTime? ServiceStartingDate { get; set; }
        public DateTime? ServiceEndingDate { get; set; }
        public DateTime? CancelingDate { get; set; }
        public bool IsExtendContract { get; set; }
        public string DeviceContractImage { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime? UpdateDateTime { get; set; }

        public Member Member { get; set; }
        public StoreClawMachinePlace Place { get; set; }
        public ServiceOrdertStatus ServiceOrdertStatus { get; set; }
        public ICollection<ServiceOrderDetail> ServiceOrderDetail { get; set; }
    }
}
