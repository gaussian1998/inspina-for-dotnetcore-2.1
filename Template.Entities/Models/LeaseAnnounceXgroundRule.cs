﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class LeaseAnnounceXgroundRule
    {
        public long GroundRuleId { get; set; }
        public long StoreId { get; set; }

        public GroundRule GroundRule { get; set; }
        public LeaseAnnounce Store { get; set; }
    }
}
