﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ProtectedServerDetail
    {
        public string Id { get; set; }
        public string ProtectedId { get; set; }
        public string ClientId { get; set; }
        public string HashValue { get; set; }
        public int AuthZtimes { get; set; }
        public int CurrentTimes { get; set; }
        public string ClientProtectedKey { get; set; }
        public string ClientProtectedIv { get; set; }
        public bool? Avaliable { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public ProtectedServer Protected { get; set; }
    }
}
