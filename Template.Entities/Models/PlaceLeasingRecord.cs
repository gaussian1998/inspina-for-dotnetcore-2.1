﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class PlaceLeasingRecord
    {
        public PlaceLeasingRecord()
        {
            PlaceLeasingService = new HashSet<PlaceLeasingService>();
        }

        public string PlaceLeasingRecordId { get; set; }
        public string PlaceLeasingRecordNumber { get; set; }
        public long PlaceId { get; set; }
        public int PlaceNumber { get; set; }
        public string DecorationTime { get; set; }
        public bool IsClose { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public StoreClawMachinePlace Place { get; set; }
        public ICollection<PlaceLeasingService> PlaceLeasingService { get; set; }
    }
}
