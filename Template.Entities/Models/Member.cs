﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Member
    {
        public Member()
        {
            MemberDevice = new HashSet<MemberDevice>();
            ServiceOrder = new HashSet<ServiceOrder>();
            TenancyReservation = new HashSet<TenancyReservation>();
        }

        public long MemberId { get; set; }
        public string MemberAccount { get; set; }
        public string IdentityNo { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Representative { get; set; }
        public bool IsCompany { get; set; }
        public DateTime? Birthday { get; set; }
        public string Tel { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Rank { get; set; }
        public string FrontofIdcardImage { get; set; }
        public string BackofIdcardImage { get; set; }
        public string MemberImage { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public ICollection<MemberDevice> MemberDevice { get; set; }
        public ICollection<ServiceOrder> ServiceOrder { get; set; }
        public ICollection<TenancyReservation> TenancyReservation { get; set; }
    }
}
