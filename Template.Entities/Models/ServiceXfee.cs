﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ServiceXfee
    {
        public long Id { get; set; }
        public long FeeRuleId { get; set; }
        public long ServiceClassId { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public FeeRule FeeRule { get; set; }
        public ServiceClass ServiceClass { get; set; }
    }
}
