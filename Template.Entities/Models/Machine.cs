﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class Machine
    {
        public Machine()
        {
            Device = new HashSet<Device>();
        }

        public long MachineId { get; set; }
        public long? MachineTypeId { get; set; }
        public string DeviceName { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNumber { get; set; }
        public int? Width { get; set; }
        public int? Depth { get; set; }
        public int? Length { get; set; }
        public int? Weight { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public MachineType MachineType { get; set; }
        public ICollection<Device> Device { get; set; }
    }
}
