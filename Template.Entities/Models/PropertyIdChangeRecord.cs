﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class PropertyIdChangeRecord
    {
        public long Picid { get; set; }
        public string OldPropertyId { get; set; }
        public string NewPropertyId { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Updator { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
