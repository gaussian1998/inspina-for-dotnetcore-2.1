﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ProtectedServer
    {
        public ProtectedServer()
        {
            ProtectedServerDetail = new HashSet<ProtectedServerDetail>();
        }

        public string ProtectedId { get; set; }
        public string AppName { get; set; }
        public string AuthProtectedKey { get; set; }
        public string AuthProtectedIv { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public ICollection<ProtectedServerDetail> ProtectedServerDetail { get; set; }
    }
}
