﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class StoreXequipment
    {
        public long StoreId { get; set; }
        public long EquipmentId { get; set; }

        public Equipment Equipment { get; set; }
        public Store Store { get; set; }
    }
}
