﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class StoreExposureDepositLog
    {
        public long StoreExposureDepositLogId { get; set; }
        public string StoreExposureDepositId { get; set; }
        public int IncreaseAmount { get; set; }
        public int DecreaseAmount { get; set; }
        public int DepositMemo { get; set; }
        public DateTime CreateDateTime { get; set; }

        public StoreExposureDeposit StoreExposureDeposit { get; set; }
    }
}
