﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class PaymentCycle
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public int? Value { get; set; }
    }
}
