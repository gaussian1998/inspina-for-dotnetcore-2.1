﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class OwnerExposureDepositLog
    {
        public long OwnerExposureDepositLogId { get; set; }
        public string OwnerExposureDepositId { get; set; }
        public int IncreaseAmount { get; set; }
        public int DecreaseAmount { get; set; }
        public int DepositMemo { get; set; }
        public DateTime CreateDateTime { get; set; }

        public OwnerExposureDeposit OwnerExposureDeposit { get; set; }
    }
}
