﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class ValidityPeriod
    {
        public ValidityPeriod()
        {
            ServiceClass = new HashSet<ServiceClass>();
        }

        public int Code { get; set; }
        public string Description { get; set; }
        public int? Value { get; set; }

        public ICollection<ServiceClass> ServiceClass { get; set; }
    }
}
