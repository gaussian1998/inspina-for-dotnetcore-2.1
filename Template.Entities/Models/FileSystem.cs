﻿using System;
using System.Collections.Generic;

namespace Template.Entities.Models
{
    public partial class FileSystem
    {
        public FileSystem()
        {
            InverseDirectoryNavigation = new HashSet<FileSystem>();
        }

        public string Id { get; set; }
        public string FileName { get; set; }
        public string Directory { get; set; }
        public bool IsDirectory { get; set; }
        public byte[] Data { get; set; }
        public long Size { get; set; }

        public FileSystem DirectoryNavigation { get; set; }
        public ICollection<FileSystem> InverseDirectoryNavigation { get; set; }
    }
}
