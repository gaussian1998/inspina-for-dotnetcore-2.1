﻿using ClassLibrary.Extensions;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace ClassLibrary
{
    public static class HttpTools
    {
        static HttpTools()
        {
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback((p1, p2, p3, p4) => true);
        }

        public static HttpWebResponse Post(string url, byte[] body, string ContentType = "application/json ; charset=utf-8")
        {
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = ContentType;
            request.ContentLength = body.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(body, 0, body.Length);
            }

            return (HttpWebResponse)request.GetResponse();
        }

        public static HttpWebResponse PostJson<T>(string url, T model)
        {
            string json = JsonConvert.SerializeObject(model);
            byte[] body = Encoding.UTF8.GetBytes(json);

            return Post(url, body);
        }

        public static HttpWebResponse Get(string url, int timeOutSec = 90)
        {
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
            request.Timeout = timeOutSec * 1000;
            request.Method = "GET";

            return (HttpWebResponse)request.GetResponse();
        }

        public static byte[] ReadBody(this WebResponse response)
        {
            using (var stream = response.GetResponseStream())
            {
                return stream.ReadToEnd();
            }
        }
    }
}
