﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace ClassLibrary
{
    public static class MapProperty
    {
        public static To Mapping<To, From>(From from)
            where To : new()
        {
            To to = new To();

            MappingHelper(to, from);
            return to;
        }

        public static IEnumerable<To> MapAll<To, From>(IEnumerable<From> froms)
            where To : new()
        {
            return froms.Select(from => Mapping<To, From>(from));
        }

        public static IEnumerable<To> MapAll<To, From>(IEnumerable<From> froms, Func<From, To> select)
            where To : new()
        {
            return froms.Select(from => select(from));
        }


        private static void MappingHelper<To, From>(To to, From from)
        {
            if (from == null) return;
            foreach (PropertyInfo fromProperty in typeof(From).GetProperties())
            {
                foreach (PropertyInfo toProperty in typeof(To).GetProperties())
                {
                    if (IsMappingPermit(toProperty, fromProperty))
                        toProperty.SetValue(to, fromProperty.GetValue(from));
                }
            }
        }

        private static bool IsMappingPermit(PropertyInfo toProperty, PropertyInfo fromProperty)
        {
            return fromProperty.Name == toProperty.Name &&
                fromProperty.PropertyType == toProperty.PropertyType &&
                toProperty.SetMethod != null &&
                fromProperty.GetMethod != null;
        }
    }
}
