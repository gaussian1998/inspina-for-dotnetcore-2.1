﻿
using ClassLibrary.Interface;

namespace ClassLibrary.Extensions
{
    /// <summary>
    /// IVisitor 範式變化版本
    /// 可變遞迴樣板試作一號機
    /// </summary>
    public static class AcceptorExtension
    {
        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this T16 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
            where T12 : class
            where T13 : class
            where T14 : class
            where T15 : class
            where T16 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this T15 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
            where T12 : class
            where T13 : class
            where T14 : class
            where T15 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this T14 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
            where T12 : class
            where T13 : class
            where T14 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this T13 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
            where T12 : class
            where T13 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this T12 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
            where T12 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this T11 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
            where T11 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this T10 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
            where T10 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this T9 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
            where T9 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8, T9>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7, T8>(this T8 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7, T8> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
            where T8 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7, T8>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6, T7>(this T7 obj, IVisitor<T1, T2, T3, T4, T5, T6, T7> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
            where T7 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6, T7>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5, T6>(this T6 obj, IVisitor<T1, T2, T3, T4, T5, T6> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
            where T6 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5, T6>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4, T5>(this T5 obj, IVisitor<T1, T2, T3, T4, T5> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
            where T5 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4, T5>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3, T4>(this T4 obj, IVisitor<T1, T2, T3, T4> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
            where T4 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3, T4>(obj, IVisitor);
        }

        public static void Accept<T1, T2, T3>(this T3 obj, IVisitor<T1, T2, T3> IVisitor)
            where T1 : class
            where T2 : class
            where T3 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2, T3>(obj, IVisitor);
        }

        public static void Accept<T1, T2>(this T2 obj, IVisitor<T1, T2> IVisitor)
            where T1 : class
            where T2 : class
        {
            if (obj is T1)
                IVisitor.Visit(obj as T1);
            else
                Accept<T2>(obj, IVisitor);
        }

        public static void Accept<T1>(this T1 obj, IVisitor<T1> IVisitor)
            where T1 : class
        {
            IVisitor.Visit(obj as T1);
        }
    }
}
