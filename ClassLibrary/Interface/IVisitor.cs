﻿
namespace ClassLibrary.Interface
{
    /// <summary>
    /// IVisitor 範式變化版本
    /// 可變遞迴樣板試作一號機
    /// </summary>
    public interface IVisitor<T1>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2> : IVisitor<T2>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3> : IVisitor<T2, T3>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4> : IVisitor<T2, T3, T4>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5> : IVisitor<T2, T3, T4, T5>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6> : IVisitor<T2, T3, T4, T5, T6>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7> : IVisitor<T2, T3, T4, T5, T6, T7>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8> : IVisitor<T2, T3, T4, T5, T6, T7, T8>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
    {
        void Visit(T1 acceptor);
    }

    public interface IVisitor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> : IVisitor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
    {
        void Visit(T1 acceptor);
    }
}
