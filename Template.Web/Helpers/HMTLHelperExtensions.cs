﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;

namespace Template.Web.Helpers
{
    public static class HMTLHelperExtensions
    {
        public static string IsSelected(this IHtmlHelper<dynamic> html, string controller = null, string action = null, string cssClass = null)
        {
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                cssClass : String.Empty;
        }

        public static string InspiniaSelected(this IHtmlHelper<dynamic> html, string action)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];

            return action == currentAction ? "active" : String.Empty;
        }

        public static string InspiniaSelected(this IHtmlHelper<dynamic> html, string[] actions)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];

            return actions.Contains(currentAction) ? "active" : String.Empty;
        }
    }
}
